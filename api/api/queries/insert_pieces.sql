insert into Piece (
        'id',
        'puzzle',
        'x',
        'y',
        'r',
        'rotate',
        'row',
        'col',
        'status',
        'bg'
) values (
        :id,
        :puzzle,
        :x,
        :y,
        :r,
        :row,
        :col,
        :rotate,
        :status,
        :bg
);
