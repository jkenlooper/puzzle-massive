import angular from 'angular'
import base from '../base'
import '../preview'
import '../gallery'
import '../queue'
import './frontpage.css'

angular.module('frontpage', [base])
  .name
