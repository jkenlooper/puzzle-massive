import angular from 'angular'
import base from '../base'
import '../queue'
import './queuepage.css'

angular.module('queuepage', [base])
  .name
