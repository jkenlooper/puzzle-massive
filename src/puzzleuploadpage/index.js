import angular from 'angular'
import base from '../base'
import '../upload-form'
import './puzzleuploadpage.css'

angular.module('puzzleuploadpage', [base])
  .name
