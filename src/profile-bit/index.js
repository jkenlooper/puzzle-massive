import angular from 'angular'
import ProfileBitController from './profile-bit.controller.js'

export default angular.module('profile-bit', [])
  .controller('ProfileBitController', ProfileBitController)
  .name
