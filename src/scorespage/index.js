import angular from 'angular'
import base from '../base'
import './scorespage.css'
import '../high-scores'
import ranking from '../ranking'

angular.module('scorespage', [base, ranking])
  .name
