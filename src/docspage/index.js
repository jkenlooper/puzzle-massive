import angular from 'angular'
import base from '../base'
import './docspage.css'

angular.module('docspage', [base])
  .name
